import os

from flask import Flask
app = Flask(__name__)

import nltk
import json

@app.route('/tag/<word>')
def tag(word):
  text = nltk.word_tokenize(word)
  return json.dumps(nltk.pos_tag(text))

@app.route('/chunk/<sentence>')
def chunk(sentence):
  sentence = nltk.word_tokenize(sentence)
  sentence = nltk.pos_tag(sentence)
  grammar = "CLAIM: {<NN>+<VBZ><RB.*>*<JJ>}"
  cp = nltk.RegexpParser(grammar)
  result = cp.parse(sentence)
  print result
  return 'Hi'

if __name__ == '__main__':
  # Bind to PORT if defined, otherwise default to 5000.
  port = int(os.environ.get('PORT', 5000))
  app.run(host='0.0.0.0', port=port, debug=True)
